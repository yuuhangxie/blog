---
title: 前端面试题汇总 -- HTML&CSS
date: 2019/07/28 22:00:00
updated: 2019/07/29 09:30:00
categories:
- 前端面试题汇总
- HTML&CSS篇
tags:
- CSS
- HTML
---

HTML&CSS：  对Web标准的理解、浏览器内核差异、兼容性、hack、CSS基本功：布局、盒模型、选择器优先级及使用、HTML5、CSS3、移动端适应。

会持续定期更新HTML + CSS + JS的面试题，并且分享题目答案，如有理解不对的地方，欢迎评论指正！



<!-- more -->



## 1. display:none和visibility:hidden的区别？

* `display: none`  隐藏对应的元素，在文档布局中不再给它分配空间。 在被设置 `display: none`后，不会占据该元素原来所在的位置，会触发回流。
* `visibility: hidden` 隐藏对应的元素，但是在文档布局中仍保留原来的空间。在被设置`visibility: hidden` 后，仍占据原来的位置，会触发重绘。



## 2. CSS中 link 和@import 的区别是？

1. link属于HTML标签，而@import是CSS提供的；
2. 页面加载时，`link`会同时被加载，而`@import`引用的CSS会等到页面被加载完再加载；
3. `@import`只在IE5以上才能识别，而`link`是HTML标签，无兼容问题；
4. 当使用JS控制`DOM`去改变样式的时候，只能使用`link`标签，因为`@import`不是`DOM`可以控制的



## 3. position的absolute与fixed共同点与不同点？

* 共同点

1. 改变行内元素的呈现方式，`display`被置为`block`；

2. 脱离文档流，不占据空间；
3. 默认会覆盖到非定位元素上。

* 不同点

1. absolute是根据设置有`position`属性的父元素来定位的，而`fixed`的”根元素“固定为浏览器窗口。
2. 滚动网页，`fixed`元素与浏览器窗口之间的距离是不变的。 



## 4. 介绍一下CSS的盒子模型？

> CSS盒子模型分为两种：W3C标准盒模型和IE怪异盒模型
>
> 盒模型的组成：边框`border`、边界/边距`margin`、补白/填充`padding`、内容区`content`。

* **标准盒模型**：`width`指`content`部分的宽度。

* **怪异盒模型**：`width`表示`content`+`padding`+`border`这三个部分的宽度。

借助css3的`box-sizing`属性切换盒模型：

```css
box-sizing: content-box /* 标准盒模型 */
box-sizing: border-box /* 怪异盒模型 */
```



## 5. CSS选择器有哪些？

* 选择器

```css
1. id选择器(#id)
2. 类选择器(.class)
3. 元素/类型/标签选择器(div, p, span)
4. 群组选择器(.class1 , .class2, .class3 , ... , classN)
5. 包含/后代选择器(parent child)
6. 子代选择器(parent > child)
7. 通配符( * )
8. 相邻兄弟选择器(div + p / div ~ p)
```

* CSS3选择器

```css
1. 属性选择器(input[type="text"])
2. 结构伪类选择器(:nth-child...)
3. UI状态伪类选择器(:disabled, :enabled, :checked)
4. 动态伪类选择器(:link, :hover, :active, :focus)
5. 否定伪类选择器(:not)
6. 目标伪类选择器(:target)
7. 语言伪类选择器(:lang)
```



## 6. 元素类型有哪几种，分别列举每种元素属于什么元素类型？

> 1. 块元素：每个块级元素独占一行；元素的高度、宽度、行高以及顶和底边距都可设置；元素宽度在不设置的情况下，是它本身父容器的100%（和父元素的宽度一致）。

```html
div,dl,dt,dd,ul,ol,li,p,h1~h6,form,table,tr,td,hr,body,html,fieldset,thead,tbody,tfoot,th,header,footer,nav,section,main,figure,figcaption,aside,article,legend
```

> 2. 内联元素：和其他元素都在一行上，即不独占一行；元素的高度、宽度及顶部和底部边距不可设置；元素的宽度就是它包含的文字或图片的宽度，不可改变。

```html
a,img,input,span,strong,i,em,b,sub,sup,del,s,u,br,mark,label,select,textarea
```

> 3. 内联块元素：和其他元素都在一行上，即不独占一行；元素的高度、宽度、行高以及顶和底边距均可设置。

```html
img,input
```



## 7. 列举display的属性值，并说明他们的作用



| 值                 |                             描述                             |
| :----------------- | :----------------------------------------------------------: |
| none               |                      此元素不会被显示。                      |
| block              |       此元素将显示为块级元素，此元素前后会带有换行符。       |
| inline             |     默认。此元素会被显示为内联元素，元素前后没有换行符。     |
| inline-block       |               行内块元素。（CSS2.1 新增的值）                |
| list-item          |                    此元素会作为列表显示。                    |
| run-in             |        此元素会根据上下文作为块级元素或内联元素显示。        |
| compact            | CSS 中有值 compact，不过由于缺乏广泛支持，已经从 CSS2.1 中删除。 |
| marker             | CSS 中有值 marker，不过由于缺乏广泛支持，已经从 CSS2.1 中删除。 |
| table              | 此元素会作为块级表格来显示（类似 `<table>`），表格前后带有换行符。 |
| inline-table       | 此元素会作为内联表格来显示（类似 `<table>`），表格前后没有换行符。 |
| table-row-group    |   此元素会作为一个或多个行的分组来显示（类似 `<tbody>`）。   |
| table-header-group |   此元素会作为一个或多个行的分组来显示（类似 `<thead>`）。   |
| table-footer-group |   此元素会作为一个或多个行的分组来显示（类似 `<tfoot>`）。   |
| table-row          |         此元素会作为一个表格行显示（类似 `<tr>`）。          |
| table-column-group | 此元素会作为一个或多个列的分组来显示（类似 `<colgroup>`）。  |
| table-column       |         此元素会作为一个单元格列显示（类似` <col>`）         |
| table-cell         |   此元素会作为一个表格单元格显示（类似 `<td> `和 `<th>`）    |
| table-caption      |       此元素会作为一个表格标题显示（类似 `<caption>`）       |
| inherit            |          规定应该从父元素继承` display` 属性的值。           |



## 8. 为什么要初始化CSS样式？

1. 因为浏览器的兼容问题，不同浏览器对有些标签的默认值是不同的，如果没对CSS初始化往往会出现浏览器之间的页面显示差异。
2. 初始化CSS样式主要是提高编码质量，如果不初始化整个页面做完很糟糕，重复的CSS样式很多。去掉标签的默认样式如：`margin`,`padding`，其他浏览器默认解析字体大小，字体设置。



## 9. 谈一下对BFC的理解？

> BFC : Block Formating Context 块级格式化上下文
>
> 它是一个独立的渲染区域，只有Block-level box参与， 它规定了内部的Block-level Box如何布局，并且与这个区域外部毫不相干。

* **渲染规则**：

1. 内部的Box会在垂直方向，一个接一个地放置；
2. Box垂直方向的距离由margin决定。属于同一个BFC的两个相邻Box的margin会发生重叠；
3. 每个元素的margin box的左边， 与包含块border box的左边相接触(对于从左往右的格式化，否则相反)。即使存在浮动也是如此；
4. BFC的区域不会与float box重叠；
5. BFC就是页面上的一个隔离的独立容器，容器里面的子元素不会影响到外面的元素。反之也如此；
6. 计算BFC的高度时，浮动元素也参与计算。

* **BFC生成规则**

1. float的值不是none；
2. position的值为absolute或fixed；
3. display的值是inline-block、table-cell、table-caption、flex或者inline-flex；
4. overflow的值不是visible；
5. 根元素html本身就是BFC

* **BFC应用形式**

1. 自适应两栏布局
2. 清除内部浮动（高度塌陷）
3. 防止垂直margin重叠



## 10. 图片整合CSS sprites的优势

CSS Sprites其实就是把网页中一些背景图片整合到一张图片文件中，再利用CSS的“background-image”，“background- repeat”，“background-position”的组合进行背景定位，background-position可以用数字精确的定位出背景图片的位置。

1. 减少了对服务器的请求次数；
2. 减小图片的体积；
3. 提升网页的加载速度；
4. 解决了网页设计师在图片命名上的困扰，只需对一张集合的图片上命名就可以了，不需要对每一个小元素进行命名；
5. 更换风格方便，只需要在一张或少张图片上修改图片的颜色或样式，整个网页的风格就可以改变。



## 11. 请列举清除浮动的几种方法

```css
1. 父元素添加 overflow:hidden;

2. 在浮动元素后加一个空的块元素，并设置 {clear:both; height:0; overflow:hidden;}

3. 万能清除浮动：为父元素添加
父元素：after{content:""; display:block; clear:both,height:0; visibility:hidden;}
父元素{zoom:1;}  /* 兼容IE */
```



## 12. IE6常见CSS解析Bug及解决办法

> 1. 在元素中直接插入图片时，图片下方会产生约3像素的间隙(该bug出现在IE6及更低版本中)

```css
hack1:将<img>转为块状元素，给<img>添加声明：display:block;

hack2:将img设置vertical-align:top/middle/bottom;只要不为baseline
```

> 2. 当IE6及更低版本浏览器在解析浮动元素时，会错误地把浮向边边界加倍显示。

```css
hack:给浮动元素添加声明：display:inline;
```

> 3. 在IE6/ie7及以下版本中，部分块元素拥有默认高度（低于16px高度）

```css
hack1:给元素添加声明：font-size:0; /* 低于大概3像素的不能正常显示； */

hack2：给元素添加声明：overflow:hidden;
```

> 4. 在IE6及以下版本中在解析百分比时，会按四舍五入方式计算从而导致50%加50%大于100%的情况。

```css
hack:给右面的浮动元素添加声明：clear:right; /* 清除右浮动。 */
```

> 5. 透明度兼容写法

```css
1.opacity:0~1;  /* IE8以上的浏览器 */

2.filter:alpha(opacity=1~100);    /* IE9及IE9以下的浏览器 */
```



## 13. CSS3有哪些新特性？

1. CSS3选择器

2. 图片的视觉效果（圆角、阴影、渐变背景、图片边框等）

3. 背景的应用（background-origin/background-clip/background-size）

4. 盒模型的变化
5. 阴影效果（文本阴影、盒子阴影）
6. 多列布局和弹性盒布局
7. web文字和font图标、颜色和透明度、圆角和边框的新特效、2D和3D变形、CSS3过渡和动画效果
8. 媒体查询和Responsive布局。

 

## 14. HTML与XHTML——二者有什么区别？

1. XHTML 元素必须被正确地嵌套
2. XHTML 元素必须被关闭，空标签也必须被关闭，如 \<br> 必须写成\<br />
3. XHTML 标签名必须用小写字母
4. XHTML 文档必须拥有根元素
5. XHTML 文档要求给所有属性赋一个值
6. XHTML 要求所有的属性必须用引号""括起来
7. XHTML 文档需要把所有 < 、>、& 等特殊符号用编码表示
8. XHTML 文档不要在注释内容中使“--”
9. XHTML 图片必须有说明文字
10. XHTML 文档中用id属性代替name属性



## 15. 请说出HTML5新增元素

**\<canvas> 新元素**

| 标签      | 描述                                                         |
| :-------- | :----------------------------------------------------------- |
| \<canvas> | 标签定义图形，比如图表和其他图像。该标签基于 JavaScript 的绘图 API |

**新多媒体元素**

| 标签      | 描述                                                         |
| :-------- | :----------------------------------------------------------- |
| \<audio>  | 定义音频内容                                                 |
| \<video>  | 定义视频（\video 或者 \movie）                               |
| \<source> | 定义多媒体资源\<video> 和 \<audio>                           |
| \<embed>  | 定义嵌入的内容，比如插件。                                   |
| \<track>  | 为诸如 \<video> 和 \<audio> 元素之类的媒介规定外部文本轨道。 |

**新表单元素**

| 标签        | 描述                                                         |
| :---------- | :----------------------------------------------------------- |
| \<datalist> | 定义选项列表。请与 input 元素配合使用该元素，来定义 input 可能的值。 |
| \<keygen>   | 规定用于表单的密钥对生成器字段。                             |
| \<output>   | 定义不同类型的输出，比如脚本的输出。                         |

**新的语义和结构元素**

| 标签          | 描述                                                         |
| :------------ | :----------------------------------------------------------- |
| \<article>    | 定义页面独立的内容区域。                                     |
| \<aside>      | 定义页面的侧边栏内容。                                       |
| \<bdi>        | 允许您设置一段文本，使其脱离其父元素的文本方向设置。         |
| \<command>    | 定义命令按钮，比如单选按钮、复选框或按钮                     |
| \<details>    | 用于描述文档或文档某个部分的细节                             |
| \<dialog>     | 定义对话框，比如提示框                                       |
| \<summary>    | 标签包含 details 元素的标题                                  |
| \<figure>     | 规定独立的流内容（图像、图表、照片、代码等等）。             |
| \<figcaption> | 定义 \<figure> 元素的标题                                    |
| \<footer>     | 定义 section 或 document 的页脚。                            |
| \<header>     | 定义了文档的头部区域                                         |
| \<mark>       | 定义带有记号的文本。                                         |
| \<meter>      | 定义度量衡。仅用于已知最大和最小值的度量。                   |
| \<nav>        | 定义导航链接的部分。                                         |
| \<progress>   | 定义任何类型的任务的进度。                                   |
| \<ruby>       | 定义 ruby 注释（中文注音或字符）。                           |
| \<rt>         | 定义字符（中文注音或字符）的解释或发音。                     |
| \<rp>         | 在 ruby 注释中使用，定义不支持 ruby 元素的浏览器所显示的内容。 |
| \<section>    | 定义文档中的节（section、区段）。                            |
| \<time>       | 定义日期或时间。                                             |
| \<wbr>        | 规定在文本中的何处适合添加换行符。                           |



## 16. iframe的优缺点？

**优点**

1. 解决加载缓慢的第三方内容如图标和广告等的加载问题；
2. `iframe`无刷新文件上传
3. `iframe`跨域通信

**缺点**

1. `iframe`会阻塞主页面的`Onload`事件；
2. 无法被一些搜索引擎索引到；
3. 页面会增加服务器的http请求；
4. 会产生很多页面，不容易管理；



## 17. 简述一下src与href的区别？

> `src`用于替换当前元素，href用于在当前文档和引用资源之间确立联系。
>
> `src`是source的缩写，指向外部资源的位置，指向的内容将会嵌入到文档中当前标签所在位置；在请求`src`资源时会将其指向的资源下载并应用到文档内，例如js脚本，img图片和frame等元素。

```html
<script src =”js.js”></script>
```

当浏览器解析到该元素时，会暂停其他资源的下载和处理，直到将该资源加载、编译、执行完毕，图片和框架等元素也如此，类似于将所指向资源嵌入当前标签内。这也是为什么将js脚本放在底部而不是头部。

> href是Hypertext Reference的缩写，指向网络资源所在位置，建立和当前元素（锚点）或当前文档（链接）之间的链接。

```html
<link href=”common.css” rel=”stylesheet”/>
```

那么浏览器会识别该文档为css文件，就会并行下载资源并且不会停止对当前文档的处理。这也是为什么建议使用link方式来加载css，而不是使用@import方式。



## 18. 请列举几种隐藏元素的方法

1. visibility: hidden；这个属性只是简单的隐藏某个元素，但是元素占用的空间任然存在。

2. opacity: 0；一个CSS3属性，设置0可以使一个元素完全透明，制作出和visibility一样的效果。与visibility相比，它可以被transition和animate

3. position: absolute；使元素脱离文档流，处于普通文档之上，给它设置一个很大的left负值定位，使元素定位在可见区域之外。
4. display: none；元素会变得不可见，并且不会再占用文档的空间。
5. transform: scale(0)；将一个元素设置为无限小，这个元素将不可见。这个元素原来所在的位置将被保留。
6. HTML5 hidden attribute；hidden属性的效果和display:none;相同，这个属性用于记录一个元素的状态。
7. height: 0; overflow: hidden；将元素在垂直方向上收缩为0,使元素消失。只要元素没有可见的边框，该技术就可以正常工作。
8.  filter: blur(0)；将一个元素的模糊度设置为0，从而使这个元素“消失”在页面中。



## 19. 请简述CSS样式表继承

> CSS样式表继承指的是，特定的CSS属性向下传递到子孙元素。会被继承下去的属性如下：

```css
文本相关：font-family，font-size， font-style，font-variant，font-weight， font，letter-spacing，line-height，color

列表相关：list-style-image，list-style-position，list-style-type， list-style
```



## 20. 请简述渐进增强和优雅降级

> 渐进增强（progressive enhancement）：一开始只构建站点的最少特性，然后不断地对不同的浏览器追加不同的功能。
> 优雅降级（graceful degradation）：一开始就构建站点的完整功能，然后针对浏览器进行测试和修复。

```css
/* 优雅降级，从大到小 */
div{
    transition: all 1s;
    -webkit-transition: all 1s;
    -o-transition: all 1s;
    -moz-transition: all 1s;
}

/* 渐进增强，从小到大 */
div{
    -webkit-transition: all 1s;
    -o-transition: all 1s;
    -moz-transition: all 1s;
    transition: all 1s;
}
```



















<br><br><br>

***to be continued***

