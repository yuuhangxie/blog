---
title: 利用DFS、BFS实现迷宫生成
date: 2019/07/13 21:20:00
updated:  2019/07/13 21:40:00
tag:
- DFS
- BFS
categories:
- 数据结构与算法
- 迷宫
---

​		利用图的遍历相关算法(深度优先搜索、广度优先搜索)，生成所需要的迷宫，但这样的迷宫太过单一，所以在此基础上对数据结构进行改进，添加随机数控制待遍历节点的入栈/出栈/入队/出队，以及对某些极个别节点的二次遍历，来实现迷宫的多解。



<!--more-->



### 思路

#### 数据结构

##### 深度优先搜索(DFS)

DFS深度优先遍历图的方法是，从图中某顶点v出发：

1. 访问顶点v；
2. 依次从v的未被访问的邻接点出发，对图进行深度优先遍历；直至图中和v有路径相通的顶点都被访问；
3. 若此时图中尚有顶点未被访问，则从一个未被访问的顶点出发，重新进行深度优先遍历，直到图中所有顶点均被访问过为止。

DFS的递归模式由于对性能影响较大，故此处利用栈(后进先出)的数据结构来模拟DFS的递归算法

```javascript
// 栈的数据结构
class Stack {
	constructor() {
        this.stack = [];
    }
    push(pos){   // 入栈
        this.stack.push(pos);
    }
    pop() {  // 出栈
        return this.stack.pop();
    }
    empty() {   // 判空，数组长度为0则为true，否则为false
        return !this.stack.length;
    }
}
```

##### 广度优先搜索(BFS)

BFS借助队列(先进先出)来实现。遍历图的方法如下：

1. 把根节点放到队列的末尾；
2. 每次从队列的头部取出一个元素，查看这个元素所有的下一级元素，把它们放到队列的末尾。并把这个元素记为它下一级元素的前驱；
3. 队列中没有节点时代表遍历结束。

```javascript
// 图的数据结构
class Queue {
    constructor() {
        this.queue = [];
    }
    push(pos){   // 入队
        this.queue.unshift(pos);
    }
    pop() {  // 出队
        return this.queue.shift();
    }
    empty() {   // 判空，数组长度为0则为true，否则为false
        return !this.queue.length;
    }
}
```

##### 模拟随机遍历

这里想借助栈和队列的进出方式的随机混用来实现随机的数据结构，故将上述两数据结构合并并加以修改，即可实现：

```javascript
// 随机进出的数据结构
class Structure {
    constructor() {
        this.structure = [];
    }
    push(pos) {   // 借助random方法来随机选择从头/尾进入数组
        if (Math.random() < 0.5)
            this.structure.push(pos);
        else
            this.structure.unshift(pos);
    }
    pop() {   // 借助random方法来随机选择从头/尾删除节点
        if (Math.random() < 0.5)
            return this.structure.pop();
        else
            return this.structure.shift();
    }
    empty() {   // 判空，数组长度为0则为true，否则为false
        return !this.structure.length;
    }
}
```

#### 数据结构封装

显然，上述三种方法存在大面积重复，故进行封装。

```javascript
//数据结构
class Structure {
    constructor() {
        this.structure = [];
    }
    push(pos) {
        if (Math.random() < Structure.patternLast)
            this.structure.push(pos);
        else
            this.structure.unshift(pos);
    }
    pop() {
        if (Math.random() < Structure.patternFirst)
            return this.structure.pop();
        else
            return this.structure.shift();
    }
    empty() {
        return !this.structure.length;
    }
    static patternLast = 0.5;
    static patternFirst = 0.5;
}
```

```javascript
//迷宫数据结构模式选择
class Pattern {
    // 模式选择：
    // fitst: 1, last: 1 模拟栈的数据结构     对应DFS
    // fitst: 0/1, last: 1/0 模拟队列的数据结构   对应BFS
    // fitst: 0.5, last: 0.5 模拟随机        默   认  
    constructor(first, last) {
        Structure.patternFirst = first;
        Structure.patternLast = last;
    }
}
```

至此，迷宫生成所需的数据结构都已构造好。下面开始迷宫创建。

#### 功能模块化

```javascript
class Maze {
    // 初始化参数
    constructor(){}
    
    // 初始化迷宫数据
    initData(maze) {}

    // 初始化迷宫DOM
    initDOM(maze) {} 

    // 初始化迷宫
    initMaze() {}
 
    // 是否打开可视化
    resetMazeShow(x, y, type) {}

    // 重新渲染迷宫 改变的格子坐标为（i, j）
    resetMaze(x, y, type) {}

    // 判断是否越界
    isArea(x, y) {}

    // 渲染迷宫
    paintMaze() {}
}
```

采用ES6的class类来编写Maze迷宫。下面的每个函数的代码都与此类中的方法对应。



### 迷宫生成

#### 初始化参数

```javascript
// row, col, 迷宫的行数 列数
// paintProgressTime, 开启可视化展示时的间隔
// width = 500, height = 500 迷宫的宽高 默认500px
// 此demo所实现的迷宫为固定出口，固定入口，行与列均为奇数
// morePath参数为0是有且只有一个解，>0时会有多解，取值0~1之间，值越大解越多，迷宫越简单
// demo中所有坐标相关的变量均代表的是第几行 第几列 从0开始
constructor(row, col, paintProgressTime = 100, morePath = 0.05, width = 500, height = 500) {
    // Maze行列
    this.row = row;
    this.col = col;
    // 迷宫的长宽
    this.width = width;
    this.height = height;
    // 设置路与墙
    this.road = ' ';
    this.wall = '#';
    // 入口坐标（1, 0）
    this.entryX = 1;
    this.entryY = 0;
    // 出口坐标（倒数第二行， 最后一列）
    this.outX = row - 2;
    this.outY = col - 1;
    // 迷宫数据
    this.maze = [];
    // 各节点的遍历情况
    this.visited = [];
    // 设置上下左右的偏移坐标值（上右下左）
    this.offset = [[-1, 0], [0, 1], [1, 0], [0, -1]];
    // 可视化展示间隔
    this.paintProgressTime = paintProgressTime;
    this.i = 0;  // 可视化展示索引
    // 多解
    this.morePath = morePath;
}
```

#### 初始化迷宫数据

```javascript
//初始化迷宫数据
initData(maze) {
    for (let i = 0; i < this.row; i++) {
        maze[i] = new Array(this.col).fill(this.wall);  // 初始化二维数组
        this.visited[i] = new Array(this.col).fill(false);  // 初始化访问状态为false
        for (let j = 0; j < this.col; j++) {
            // 横纵坐标均为奇数是路
            if (i % 2 === 1 && j % 2 === 1) {
                maze[i][j] = this.road;
            }
        }
    }
    //入口出口也是路
    maze[this.entryX][this.entryY] = this.road;
    maze[this.outX][this.outY] = this.road;
	// 返回构造的maze二位数组
    return maze;
}
```

#### 初始化迷宫DOM

```javascript
//初始化迷宫DOM
initDOM(maze) {
    let mazeDiv = document.createElement("div");  // 创建迷宫元素
    Object.assign(mazeDiv.style, {   // 设置迷宫的宽高，页面居中等属性。可根据需要删减。
        width: this.width + "px",
        height: this.height + "px",
        display: "flex",
        flexWrap: "wrap",
        marginBottom: "20px",
        margin: "50px auto 20px"
    })
    for (let i = 0; i < maze.length; i++) {   // 对maze进行渲染
        for (let j = 0; j < maze[i].length; j++) {
            let mazeSpan = document.createElement("span");  
            mazeSpan.dataset.index = i + '-' + j;
            Object.assign(mazeSpan.style, { // 计算一个格子的宽高，如果是路，则浅蓝色，墙为白色
                width: (this.width / this.col).toFixed(2) + "px",  
                height: (this.height / this.row).toFixed(2) + "px",
                background: maze[i][j] === this.wall ? "#4facfe" : "#fff"
            })
            mazeDiv.appendChild(mazeSpan);  // 将每个小格子插入大的迷宫盒子中
        }
    }
    document.body.appendChild(mazeDiv);  // 将迷宫放入页面中
}
```

#### 初始化迷宫

```javascript
// 初始化迷宫
initMaze() {
    // 迷宫数据
    let maze = this.initData(this.maze);
    // 初始化迷宫DOM
    this.initDOM(maze);
}
```

初始化生成的迷宫如下图所示

![InitMaze](https://www.xyhthink.com/img/initmaze.png)

#### 判断越界

```javascript
// 判断是否越界
isArea(x, y) {
    return x > 0 && x < this.row - 1 && y > 0 && y < this.col - 1;
}
```

#### 渲染迷宫(主函数)

```javascript
// 渲染迷宫
paintMaze() {
    //初始化迷宫
    this.initMaze();

    let queue = new Structure();  // 生成一个随机/栈/队列 （queue不仅指队列）
    // 起点是入口右侧的点(1,1),将起点放入数组中
    queue.push({  
        x: this.entryX,
        y: this.entryY + 1
    })
    //visited置为true
    this.visited[this.entryX][this.entryY + 1] = true;
    // 通过遍历目前的白色方块，然后按照某种方法连通两个方块 即把两者之间的蓝色方块变成白色
    while (!queue.empty()) {
        let currentPos = queue.pop();  // 每次从数组中按照某种数据结构取出一个节点
        for (let i = 0; i < 4; i++) {  // 按照左下右上的次序依次访问
            // this.offset = [[-1, 0], [0, 1], [1, 0], [0, -1]];
            let newX = currentPos.x + this.offset[i][0] * 2;  //两步，把中间的墙变成路
            let newY = currentPos.y + this.offset[i][1] * 2;
            // 坐标没有越界 而且 没有被访问过
            if (this.isArea(newX, newY)) {
                if (!this.visited[newX][newY]) {
                    this.resetMazeShow((newX + currentPos.x) / 2, (newY + currentPos.y) / 2, this.road);  // 墙 => 路
                    queue.push({   // 新节点进入数组
                        x: newX,
                        y: newY
                    })
                    this.visited[newX][newY] = true;  //标记此位置已访问过
                }
                else if (Math.random() < this.morePath) {   // 此处通过对访问过的节点再次渲来实现为设置迷宫多解
                    this.resetMazeShow((newX + currentPos.x) / 2, (newY + currentPos.y) / 2, this.road);
                }
            }
        }
    }
    return this;
}
```

#### 可视化实现

```javascript
//是否打开可视化
resetMazeShow(x, y, type) {
    //不需要则正常渲染
    if (!this.paintProgressTime) {
        this.resetMaze(x, y, type);
        return false;
    }
    this.i++;  //可视化
    //利用异步队列特性来实现可视化
    setTimeout(() => {
        this.resetMaze(x, y, type);
        // console.log(2);
    }, this.i * this.paintProgressTime)
}
```

#### 重新渲染迷宫

```javascript
// 重新渲染迷宫 改变的格子坐标为（i, j）
resetMaze(x, y, type) {
    // 只有不越界才做处理
    if (this.isArea(x, y)) {
        //改变maze中的type
        this.maze[x][y] = type;
        // 改变dom节点的颜色
        let changeSpan = document.querySelector(`span[data-index="${x}-${y}"]`);
        changeSpan.style.background = type === this.wall ? "#4facfe" : "#fff";
    }
}
```

### 深度优先搜索生成迷宫

```javascript
let pattern = new Pattern(1, 1);  // 生成栈的数据结构
let maze = new Maze(25, 25, 100, 0, 600, 600).paintMaze();  // 绘制25*25的迷宫
```

![DFS迷宫生成](https://www.xyhthink.com/img/DFScreate.gif)

### 广度优先搜索生成迷宫

```javascript
let pattern = new Pattern(0, 1);   // 生成队列的数据结构
let maze = new Maze(25, 25, 100, 0, 600, 600).paintMaze();
```

![DFS生成](https://www.xyhthink.com/img/BFScreate.gif)

### 随机迷宫生成-单解

```javascript
let pattern = new Pattern(0.5, 0.5);  // 生成随机的数据结构
let maze = new Maze(25, 25, 100, 0, 600, 600).paintMaze(); 
```

![随机生成](http://www.xyhthink.com/img/random1.gif)

### 随机迷宫生成-多解

```javascript
let pattern = new Pattern(0.5, 0.5);
let maze = new Maze(25, 25, 100, 0.05, 600, 600).paintMaze();  // 改变morePath的参数实现复杂多解
```

![](http://www.xyhthink.com/img/random2.gif)

### TODO

#### 利用DFS 、BFS 、DFS+回溯实现迷宫求解

### 参考文献

* **百度百科**
* **数据结构与算法**

