---
title: 如何在CentOS中安装git
date: 2019/07/26 15:00:00
updated: 2019/07/26 15:00:00
categories:
- 服务器部署
tags:
- git
---

CentOS系统的服务器下配置安装git。



<!-- more -->



## 前提

管理员权限

所有的命令行都只能一行一行输入

Shift + Insert 键粘贴

## 安装git

1. 下载编译工具 

```shell
yum -y groupinstall "Development Tools"
```

2. 下载依赖包

```shell
yum -y install zlib-devel perl-ExtUtils-MakeMaker asciidoc xmlto openssl-devel
```

3. 下载 Git 最新版本的源代码

登录`https://github.com/git/git/releases`查看git的最新版。不要下载带有`-rc`后缀的。

在最新版本的后缀为tar.tz位置右键复制链接地址。比如我的：`https://github.com/git/git/archive/v2.22.0.tar.gz`

然后在你的命令行输入`wget `+ 链接地址

```shell
wget https://github.com/git/git/archive/v2.22.0.tar.gz
```

4. 输入`ls`查看当前下载的git文件，找到v2.xx.x.tar.gz文件名记住
5. 解压

```shell
tar -zxvf v2.22.0.tar.gz
```

6. 进入目录配置，输入ls查看解压后的文件夹

```shell
cd git-2.22.0
```

7. 编辑安装

```shell
make configure
```

```shell
./configure --prefix=/usr/local/git --with-iconv=/usr/local/libiconv
```

```shell
make && make install
```

8. 修改环境变量

```shell
vim /etc/profile
```

方向按键控制光标移动到文件最后一行，输入`i`进入编辑模式。在最后一行添加

```shell
export PATH=/usr/local/git/bin:$PATH
```

`ESC` + `:wq`保存并退出

9. 保存后使其立即生效

```shell
source /etc/profile
```

10. 查看是否安装完成

```shell
git --version
```


