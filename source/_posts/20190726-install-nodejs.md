---
title: 如何在CentOS中快速安装nodeJS
date: 2019/07/26 12:50:00
updated: 2019/07/26 13:50:00
categories:
- 服务器部署
tags:
- nodeJS
---

服务器上如果选择解压、编译的方法安装nodeJS，不仅需要gcc，而且编译时间需要很久，基本上30分钟才可以编译完成，不利于新手操作。故本文选择一种快捷、简单的方法安装nodeJS。



<!-- more -->



## 前提

管理员权限

所有的命令行都只能一行一行输入

Shift + Insert 键粘贴

## 安装EPEL

首先确定系统是否安装epel-release包

```shell
yum info epel-release
```

如果输出有关epel-release的已安装信息，则说明已经安装

```shell
[root@iz2zeey0hi6lyyicdhfqk0z ~]# yum info epel-release
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
Installed Packages
Name        : epel-release
Arch        : noarch
Version     : 7
Release     : 11
Size        : 24 k
Repo        : installed
From repo   : epel
Summary     : Extra Packages for Enterprise Linux repository configuration
URL         : http://download.fedoraproject.org/pub/epel
License     : GPLv2
Description : This package contains the Extra Packages for Enterprise Linux (EPEL) repository
            : GPG key as well as configuration for yum.

```

2. 如果没有输出详细信息，那么则安装

```shell
yum install epel-release -y
```



## 安装nodeJS

安装完epel-release后，开始安装yum软件包的nodeJS，命令行输入下面指令

```shell
yum install nodejs
```

安装完成后，验证是否正确安装，输入

```shell
node -v
// v6.30.0
```

如果输出了上面的版本信息v6.30.0，版本可能不同，则说明成功安装。



## 更新nodeJS

你会发现安装的nodeJS版本过低，这是因为yum安装的软件包更新过慢。所以我们要手动更新成最新的nodeJS版本

### 安装n

> n是nodeJS管理工具

```shell
npm install -g n
```

安装完n之后输入下面命令，安装最新的nodeJS

```shell
n latest
```

因为你现在有两个nodeJS版本，所以需要切换到最新版本，输入n查看当前版本信息。

我这里下载的最新版本是v12.6.0，上下按键选择到最新版本的选项，回车。

回到主界面之后，输入 node -v 可能出现下面的报错

```shell
-bash: git: command not found  // 系统报错信息
```

这时候就需要修改环境变量。

### 环境变量

编辑环境配置文件

```shell
vim ~/.bash_profile
```

输入i变为编辑状态，上下左右按键将光标移动文件底部，在export PATH上一行，添加下面两行代码

```shell
exportN_PREFIX=/usr/local/bin/node
exportPATH=$N_PREFIX/bin:$PATH
```

ESC取消编辑状态，输入:wq保存更改并退出编辑。

执行source使修改生效

```shell
source ~/.bash_profile
```

之后node -v查看node版本号就成功切换到最新版本。

## 参考文献

**在centos7安装nodejs并升级nodejs到最新版本**， <a href= "<https://www.jianshu.com/p/2da3e3c6b8ac>">[古德拉克的一天](https://www.jianshu.com/u/d7d85acaec75) </a>