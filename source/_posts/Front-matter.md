---
title: Front-matter
date: 2019/7/9 19:20:11
updated: 2019/7/10 12:05:21
categories:
- hexo
tags:
- front-matter
top: false
---

Front-matter 是文件最上方以 `---` 分隔的区域，用于指定个别文件的变量



<!-- more -->



举例来说：

```markdown
---
title: Hello World
date: 2019/7/9 19:20:11
---
```

还有其他参数可以选择

|              |                      |              |
| :----------- | :------------------- | :----------- |
| 参数         | 描述                 | 默认值       |
| `layout`     | 布局                 |              |
| `title`      | 标题                 |              |
| `date`       | 建立日期             | 文件建立日期 |
| `updated`    | 更新日期             | 文件更新日期 |
| `comments`   | 开启文章的评论功能   | true         |
| `tags`       | 标签（不适用于分页） |              |
| `categories` | 分类（不适用于分页） |              |
| `permalink`  | 覆盖文章网址         |              |

---

除了 YAML 外，你也可以使用 JSON 来编写 Front-matter，只要将 `---` 代换成 `;;;` 即可。

```markdown
"title": "Hello World",
"date": "2013/7/13 20:46:25"
;;;
```

Front-matter 是文件最上方以 `---` 分隔的区域，用于指定个别文件的变量。【详见官方文档】

| 字段        | 含义                 | 值类型        | 默认值        |
| :---------- | :------------------- | :------------ | :------------ |
| layout      | 布局模版             | String        | -             |
| title       | 标题                 | String        | -             |
| date        | 创建时间             | Date          | 文件创建时间  |
| updated     | 更新日期             | Date          | 文件修改时间  |
| permalink   | 覆盖文章网址         | String        | -             |
| music       | 内部音乐控件         | 详见【music】 | -             |
| keywords    | 页面关键词           | String        | -             |
| description | 页面描述、摘要       | String        | -             |
| author      | 作者                 | String        | config.author |
| author_url  | 作者链接             | String        | config.url    |
| avatar      | 作者头像             | String        | config.avatar |
| cover       | 是否显示封面         | Bool          | true          |
| meta        | 文章或页面的meta信息 | Bool, Array   | theme.meta    |
| sidebar     | 页面侧边栏           | Bool, Array   | theme.sidebar |
| body        | 页面主体元素         | Array         | theme.body    |

**layout=post时特有的字段：**

| 字段          | 含义         | 值类型        | 默认值 |
| :------------ | :----------- | :------------ | :----- |
| categories    | 分类         | String, Array | -      |
| tag           | 标签         | String, Array | -      |
| toc           | 是否生成目录 | Bool          | true   |
| popular_posts | 显示推荐文章 | Bool          | true   |
| mathjax       | 是否渲染公式 | Bool, String  | false  |
| top           | 是否置顶     | Bool          | false  |
| thumbnail     | 缩略图       | String        | false  |

**layout=links时特有的字段：**

| 字段  | 含义 | 值类型        | 默认值 |
| :---- | :--- | :------------ | -----: |
| links | 友链 | 详见【links】 |      - |





如果某个页面不需要侧边栏，可以这样写：

```
---
sidebar: false
---
```



某个页面想指定显示某几个侧边栏，就这样写:

```
---
sidebar: [grid, toc, tags] # 放置任何你想要显示的侧边栏部件
---
```



由于支持多作者维护，所以可以设置单独一篇文章的作者：

```
---
author: 另一个作者的名字
author_url: 另一个作者的主页
avatar: 另一个作者的头像
---
```



公式渲染

| 取值     | 含义                               |
| :------- | :--------------------------------- |
| false    | 不渲染，默认值                     |
| true     | 渲染                               |
| internal | 只在文章内部渲染，文章列表中不渲染 |

```
---
mathjax: true
---
```