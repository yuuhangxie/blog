---
title: 函数节流(throttle)和函数去抖(debounce)
date: 2019/07/16 08:50:00
updated: 2019/07/16 09:50:00
categories:
- JavaScript
tags:
- 函数节流
- 函数去抖
---

以下场景往往由于事件频繁被触发，因而频繁执行DOM操作、资源加载等重行为，导致UI停顿甚至浏览器崩溃。

1. window对象的resize、scroll事件

2. 拖拽时的mousemove事件

3. 射击游戏中的mousedown、keydown事件

4. 文字输入、自动完成的keyup事件

实际上对于window的resize事件，实际需求大多为停止改变大小n毫秒后执行后续处理；而其他事件大多的需求是以一定的频率执行后续处理。针对这两种需求就出现了debounce和throttle两种解决办法。



<!--more-->



#### 函数去抖 debounce

> 函数调用n秒后才会执行，如果函数在n秒内被调用的话则函数不执行，重新计算执行时间。

```javascript
function debounce(callback, delay) {
    var timer = null;  // 闭包延长timer生命周期
    return () => {
        clearTimeout(timer);  // 重复调用清空上一次计时器，重新计时
        timer = setTimeout( () => {
            callback();   // 主体回调函数
        }, delay)
    }
}
window.addEventListener("scroll",debounce(isload,80));
```



#### 函数节流 throttle

> 预先设定一个执行周期，当调用动作的时刻大于等于执行周期则执行该动作，然后进入下一个新周期。
>
> 如果将水龙头拧紧直到水是以水滴的形式流出，那你会发现每隔一段时间，就会有一滴水流出。

```javascript
function throttle(callback, delay) {
    var timer = null;  // // 闭包延长timer生命周期
    return () => {
        if(timer != null) return;  // 如果计时器已经开启，则不继续执行
        timer = setTimeout( () => {
            callback();   // 主体回调函数
        }, delay)
    }
}
window.addEventListener("scroll",throttle(isload,80));
```



#### 参考文献

* **函数节流和函数去抖**，<a href = "https://www.cnblogs.com/caizhenbo/p/6378202.html">caizhenbo</a>
* **节流和去抖**，<a href = "https://www.jianshu.com/u/b868db9f7847">杨怀智</a>

