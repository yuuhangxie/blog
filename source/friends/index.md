---
layout: links
title: 我的朋友们

links:
  - group: friends list
    icon: fas fa-handshake
    items:
    - name: xaoxuu’s blog
      avatar: https://cdn.jsdelivr.net/gh/xaoxuu/assets@master/avatar/avatar.png
      url: 'https://xaoxuu.com'
      backgroundColor: '#fff'
      textColor: '#666'
      tags:
      - iOS
    - name: love421
      avatar: https://zcjun.oss-cn-shenzhen.aliyuncs.com/o_1de42c3h9dl99uds5o1h8ha0oa.jpg
      url: 'https://www.makedreamsir.xyz'
      backgroundColor: '#fff'
      textColor: '#666'
      tags:
      - 人工智能  智能家居
    - name: jiangxin's blog
      avatar: https://www.jiangxinvictory.com/images/blog_avatar.jpeg
      url: 'https://www.jiangxinvictory.com/'
      backgroundColor: '#fff'
      textColor: '#666'
      tags:
      - Javascript
      - Linux

  - group: welcome to visit my blog
    icon: fas fa-handshake
    items:
    - name: '<i class="fas fa-comment fa-fw" aria-hidden="true"></i> 赶快留言吧'
      avatar: https://www.xyhthink.com/img/head.jpg
      url: '#comments'
      backgroundColor: '#869989'
      textColor: '#FFFD'
      tags:
      - 1~4个标签
      - 两个最佳
---

<br>

各位大佬想交换友链的话可以在下方留言，必须要有名称、头像链接、和至少一个标签哦～

> 名称： xyhthink's blog
头像： https://www.xyhthink.com/img/head.jpg
网址： https://xyhthink.com
标签： JS / C/C++ / JAVA 
