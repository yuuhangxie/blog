---
layout: page
title: 关于
body: [article, grid, comments]
meta:
  header: false
  footer: false
valine:
  placeholder: 有什么想对我说的呢 ？
sidebar: false
---

{% raw %}<div class="style-example example">{% endraw %}
<ul class="pure circle center about"><li><img  src="https://www.xyhthink.com/img/head.jpg"></li></ul>

<hr>
<br>
<center>前端攻城狮，喜欢写代码和各种风格的纯音乐/电影，有点宅，很幽默。</center><center>QQ: 3064149261</center>
<center>WeChat: xie0806</center>
<center>Email: yuuhangxie@gmail.com</center>
<center>微博: 明明 Missyou</center>
<br>











{% raw %}</div>{% endraw %}

<hr><br>
<br><br>

欢迎留言～

[1]: /about/
[2]: http://www.xyhthink.com/
[3]: https://github.com/yuuhangxie
[4]: https://gitee.com/yuuhangxie
[5]: https://music.163.com/#/user/home?id=502284444
[6]: mailto:yuuhangxie@gmail.com
